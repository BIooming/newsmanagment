package by.epam.lab.fedarenka.newsmanagment.dal.factoryimpl;

import org.dbunit.DBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import java.io.FileInputStream;

/**
 * Created by blooming on 24.6.16.
 */
public class TestOracleAuthorDao extends DBTestCase {

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("dataset.xml"));
    }
}
