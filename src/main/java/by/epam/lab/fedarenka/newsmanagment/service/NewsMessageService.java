package by.epam.lab.fedarenka.newsmanagment.service;

import by.epam.lab.fedarenka.newsmanagment.bean.NewsMessage;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.CommentDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.TagDto;

import java.util.List;

/**
 * Identity of the class of an NewsMessageService perform saving and reading
 * news message with author and tags in one step.
 */
public interface NewsMessageService {
    /**
     * Adds news message which contains news, its' author and tags.
     * Method initialize call of dao methods for each entity and returns
     * updated news message with specified id for every inner entity.
     *
     * @param newsMessage wrapper class containing news, tags and author.
     * @return wrapper class NewsMessage with instances of tags, author and
     */
    NewsMessage addNewsMessage(NewsMessage newsMessage);

    /**
     * Adds new message which contatin
     *
     * @param newsMessage
     * @return
     */
    NewsMessage updateNewsMessage(NewsMessage newsMessage);

    /**
     *
     * @param newsId
     * @return
     */
    NewsMessage getNewsMessage(Long newsId);

    /**
     *
     * @return
     */
    List<NewsMessage> getAllNewsMessages();

    List<CommentDto> getAllCommentsForNews(Long newsId);

    List<AuthorDto> getAuthorsForNews(Long newsId);

    List<TagDto> getAllTagsForNews(Long newsId);

    void deleteNewsMessage(Long newsId);

    void deleteNewsMessage(NewsMessage newsMessage);
}
