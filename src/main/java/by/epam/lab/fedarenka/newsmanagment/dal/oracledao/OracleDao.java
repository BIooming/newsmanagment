package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.dal.DalException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by blooming on 23.6.16.
 */
abstract class OracleDao {
    private DataSource dataSource;

    /**
     * Method contain common sql-request execution logic which can be used by any class
     *
     * @param sql
     * @param params
     * @return
     * @throws DalException
     */
    public ResultSet executeSqlRequest(String sql, Object... params) throws DalException {
        try {
            ResultSet resultSet = null;
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            int i = 0;
            for (Object param : params) {
                i++;
                preparedStatement.setObject(i, param);
            }
            resultSet = preparedStatement.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
