package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.TagDto;
import by.epam.lab.fedarenka.newsmanagment.dal.DalException;
import by.epam.lab.fedarenka.newsmanagment.dal.TagDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleTagDao extends OracleDao implements TagDao {
    private static final String SQL_SEARCH_BY_ID = "SELECT * FROM Tag WHERE TAG_ID=?;";
    private static final String SQL_INSERT = "INSERT INTO Tag(TAG_NAME) VALUES(?);";
    private static final String SQL_UPDATE = "UPDATE Tag SET TAG_NAME=?;";
    private static final String SQL_SEARCH_ALL = "SELECT * FROM Tag;";
    private static final String SQL_DELETE = "DELETE FROM Tag WHERE TAG_ID=?;";

    private static final String STR_TAG_ID = "TAG_ID";
    private static final String STR_TAG_NAME = "TAG_NAME";

    private static final int NULL = 0;

    public TagDto getTag(Long id) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_BY_ID,id);
        TagDto tag = getTagFromResultSet(resultSet).get(NULL);
        return tag;
    }

    public TagDto create(TagDto tag) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_INSERT, tag.getName());
        tag = getTagFromResultSet(resultSet).get(NULL);
        return tag;
    }

    public TagDto update(TagDto tag) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_UPDATE, tag.getName());
        tag = getTagFromResultSet(resultSet).get(NULL);
        return tag;
    }

    public List<TagDto> listTags() throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_ALL);
        List<TagDto> tags = getTagFromResultSet(resultSet);
        return tags;
    }

    public void delete(Long id) throws DalException {
        executeSqlRequest(SQL_DELETE);
    }

    private List<TagDto> getTagFromResultSet(ResultSet resultSet) throws DalException {
        try {
            List<TagDto> tagList = new ArrayList<TagDto>();
            while (resultSet.next()) {
                TagDto tag = new TagDto();
                tag.setId(resultSet.getLong(STR_TAG_ID));
                tag.setName(resultSet.getString(STR_TAG_NAME));
                listTags().add(tag);
            }
            return tagList;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }
}
