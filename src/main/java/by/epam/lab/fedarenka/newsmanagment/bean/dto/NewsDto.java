package by.epam.lab.fedarenka.newsmanagment.bean.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by blooming on 22.6.16.
 */
public class NewsDto implements Serializable {
    private static final Long serialVersionUID = 1L;
    private Long id;
    private String title;
    private String shortTitle;
    private String fullText;
    private Timestamp creationDate;
    private Date modificationDate;

    public NewsDto(Long id, String title, String shortTitle, String fullText, Timestamp creationDate, Date modificationDate) {
        this.id = id;
        this.title = title;
        this.shortTitle = shortTitle;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public NewsDto() {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsDto newsDto = (NewsDto) o;

        if (id != null ? !id.equals(newsDto.id) : newsDto.id != null) return false;
        if (title != null ? !title.equals(newsDto.title) : newsDto.title != null) return false;
        if (shortTitle != null ? !shortTitle.equals(newsDto.shortTitle) : newsDto.shortTitle != null) return false;
        if (fullText != null ? !fullText.equals(newsDto.fullText) : newsDto.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(newsDto.creationDate) : newsDto.creationDate != null)
            return false;
        return modificationDate != null ? modificationDate.equals(newsDto.modificationDate) : newsDto.modificationDate == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortTitle != null ? shortTitle.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortTitle='" + shortTitle + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
}
