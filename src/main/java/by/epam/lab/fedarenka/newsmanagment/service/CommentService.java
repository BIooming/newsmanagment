package by.epam.lab.fedarenka.newsmanagment.service;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.CommentDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;

import java.util.List;

/**
 * Defines common behavior of comments processing service.
 * Perform basic CRUD operations with Comment entity.
 *
 * The getAllCommentsForNews methods return full list of comments
 * for specific news message.
 *
 * @see CommentDto
 * @see List
 */
public interface CommentService {

    /**
     * Adds new comment into the database.
     *
     * @param comment entity of Comment which will be added into the database.
     * @return added Comment entity with specified id.
     */
    CommentDto addComment(CommentDto comment);

    /**
     * Updates comment data in the database. Replaces existing values of Comment fields in database
     * by new ones from the Comment entity given in parameter.
     *
     * @param comment contain new data which will replace existing in database.
     * @return updated Comment.
     */
    CommentDto updateComment(CommentDto comment);

    /**
     * Performs search in database by corresponding id given in parameter.
     *
     * @param commentId id of required news.
     * @return {@link CommentDto} entity of required Comment.
     */
    CommentDto getComment(Long commentId);

    /**
     * Returns list of all comments that relate to the news which id was
     * specified in the parameter.
     *
     * @param newsId the id of news for which method will search comments.
     * @return list of comments related to the news
     */
    List<CommentDto> getAllCommentsForNews(Long newsId);

    /**
     * Returns list of all comments that relate to the news which id was
     * specified in the parameter.
     *
     * @param news entity of news for which method will search comments.
     * @return list of comments related to the news
     */
    List<CommentDto> getAllCommentsForNews(NewsDto news);

    /**
     * Deletes comment from database by id given in parameter.
     *
     * @param commentId id of comment to delete.
     */
    void deleteComment(Long commentId);

    /**
     * Deletes comment from database.
     *
     * @param comment entity of comment to delete. Should contain id of comment.
     */
    void deleteComment(CommentDto comment);

}
