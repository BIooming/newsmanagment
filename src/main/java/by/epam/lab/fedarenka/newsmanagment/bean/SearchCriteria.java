package by.epam.lab.fedarenka.newsmanagment.bean;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.TagDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public class SearchCriteria implements Serializable {
    private static final Long serialVersionUID = 1L;
    private AuthorDto author;
    private List<TagDto> tags;

    public SearchCriteria(AuthorDto author, List<TagDto> tags) {
        this.author = author;
        this.tags = tags;
    }

    public SearchCriteria(AuthorDto author) {
        this.author = author;
    }

    public SearchCriteria(List<TagDto> tags) {
        this.tags = tags;
    }

    public SearchCriteria() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        return tags != null ? tags.equals(that.tags) : that.tags == null;

    }

    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "author=" + author +
                ", tags=" + tags +
                '}';
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDto author) {
        this.author = author;
    }

    public List<TagDto> getTags() {
        return tags;
    }

    public void setTags(List<TagDto> tags) {
        this.tags = tags;
    }
}
