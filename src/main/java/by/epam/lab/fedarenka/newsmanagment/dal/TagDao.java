package by.epam.lab.fedarenka.newsmanagment.dal;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.TagDto;

import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public interface TagDao {

    TagDto getTag(Long id) throws DalException;

    TagDto create(TagDto tag) throws DalException;

    TagDto update(TagDto tag) throws DalException;

    List<TagDto> listTags() throws DalException;

    void delete(Long id) throws DalException;
}
