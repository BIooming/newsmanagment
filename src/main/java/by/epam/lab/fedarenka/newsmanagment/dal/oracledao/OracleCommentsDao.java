package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.CommentDto;
import by.epam.lab.fedarenka.newsmanagment.dal.CommentsDao;
import by.epam.lab.fedarenka.newsmanagment.dal.DalException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleCommentsDao extends OracleDao implements CommentsDao {
    private static final String SQL_SEARCH_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT,CREATION_DATE FROM Comments WHERE NEWS_ID=?;";
    private static final String SQL_SEARCH_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT,CREATION_DATE FROM Comments WHERE COMMENT_ID=?;";
    private static final String SQL_SEARCH_ALL = "SELECT * FROM Comments;";
    private static final String SQL_CREATE = "INSERT INTO Comments(NEWS_ID,COMMENT_TEXT, CREATION_DATE) VALUES(?,?,?);";
    private static final String SQL_UPDATE = "UPDATE Comments SET NEWS_ID=?,COMMENT_TEXT=?,CREATION_DATE=? WHERE COMMENT_ID=?;";
    private static final String SQL_DELETE = "DELETE COMMENT_ID, NEWS_ID, COMMENT_TEXT,CREATION_DATE FROM Comments WHERE COMMENT_ID=?";

    private static final String STR_COMMENT_ID = "COMMENT_ID";
    private static final String STR_NEWS_ID = "NEWS_ID";
    private static final String STR_COMMENT_TEXT = "COMMENT_TEXT";
    private static final String STR_CREATION_DATE = "CREATION_DATE";

    private static final int NULL = 0;

    public CommentDto getComment(Long id) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_BY_ID, id);
        CommentDto comment = getCommentsFromResultSet(resultSet).get(NULL);
        return comment;
    }

    public CommentDto create(CommentDto comment) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_CREATE,
                comment.getNewsId(),comment.getText(),comment.getCreationDate());
        comment = getCommentsFromResultSet(resultSet).get(NULL);
        return comment;
    }

    public CommentDto update(CommentDto comment) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_UPDATE,
                comment.getNewsId(),comment.getText(),comment.getCreationDate(), comment.getId());
        comment = getCommentsFromResultSet(resultSet).get(NULL);
        return comment;
    }

    public List<CommentDto> listComments() throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_ALL);
        List<CommentDto> comments = getCommentsFromResultSet(resultSet);
        return comments;
    }

    public List<CommentDto> listCommentsForNews(Long newsId) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_BY_NEWS_ID, newsId);
        List<CommentDto> comments = getCommentsFromResultSet(resultSet);
        return comments;
    }

    public void delete(Long id) throws DalException {
        executeSqlRequest(SQL_DELETE);
    }

    private List<CommentDto> getCommentsFromResultSet(ResultSet resultSet) throws DalException {
        try {
            List<CommentDto> comments = new ArrayList<CommentDto>();
            while (resultSet.next()) {
                CommentDto comment = new CommentDto();
                comment.setId(resultSet.getLong(STR_COMMENT_ID));
                comment.setNewsId(resultSet.getLong(STR_NEWS_ID));
                comment.setText(resultSet.getString(STR_COMMENT_TEXT));
                comment.setCreationDate(resultSet.getTimestamp(STR_CREATION_DATE));
                comments.add(comment);
            }
            return comments;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }
}
