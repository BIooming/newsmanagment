package by.epam.lab.fedarenka.newsmanagment.dal;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.CommentDto;

import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public interface CommentsDao {

    CommentDto getComment(Long id) throws DalException;

    CommentDto create(CommentDto comment) throws DalException;

    CommentDto update(CommentDto comment) throws DalException;

    List<CommentDto> listComments() throws DalException;

    List<CommentDto> listCommentsForNews(Long newsId) throws DalException;

    void delete(Long id) throws DalException;
}
