package by.epam.lab.fedarenka.newsmanagment.dal;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;

import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public interface NewsDao {

    NewsDto create(NewsDto news) throws DalException;

    NewsDto getNews(Long newsId) throws DalException;

    NewsDto update(NewsDto news)  throws DalException;

    List<NewsDto> listAllNews() throws DalException;

    int count () throws DalException;

    void delete(Long newsId) throws DalException;
}
