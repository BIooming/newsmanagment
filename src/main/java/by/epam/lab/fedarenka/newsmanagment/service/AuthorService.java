package by.epam.lab.fedarenka.newsmanagment.service;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;

import java.util.List;

/**
 * Identity of the class of an AuthorService instance can perform CRUD operations
 * with Author dto. Author can not be deleted from database permanently, but
 * can be set as expired by using delete method.
 *
 * Marked as an expired author will not be given as an available to set in new news message,
 * but still be visible in author list of already created messages.
 *
 * @see AuthorDto
 */
public interface AuthorService {

    /**
     * Adds new author into the database.
     *
     * @param author instance of the Author which will be saved in database.
     * @return saved author with specified id.
     */
    AuthorDto addAuthor(AuthorDto author);

    /**
     * Updates author data in the database by replacing existing values with
     * new ones from entity given in parameter.
     *
     * @param author entity with new field values. Should have same id
     *               value as an old version.
     * @return updated Author entity.
     */
    AuthorDto updateAuthor(AuthorDto author);

    /**
     * Search Author in database by corresponding id given in parameter
     *
     * @param authorId id of author to be found in database
     * @return {@link AuthorDto} entity fully filled with data from database
     */
    AuthorDto getAuthor(Long authorId);

    /**
     * Returns list of all authors stored in database in the moment.
     *
     * @return List of {@link AuthorDto} entities filled with data from database.
     */
    List<AuthorDto> getAllAuthors();

    /**
     * Delete connection between author and news by deleting foreign keys
     * in News_Author table that link entities in database.
     *
     * @param author entity of the author to be detached. Should contain id.
     * @param news entity of the news. Should contain id.
     */
    void detachAuthorFromNews(AuthorDto author, NewsDto news);

    /**
     * Delete connection between author and news by deleting foreign keys
     * in News_Author table that link entities in database.
     *
     * @param authorId id of author to be detached.
     * @param newsId id of news.
     */
    void detachAuthorFromNews(Long authorId, Long newsId);

    /**
     * Sets authors' expire date as current date. After that author
     * counts as deleted and cannot be added as an author in new news.
     * But it still be possible to see author in already created news messages.
     *
     * @param authorDto entity of author to delete. Should contain id.
     */
    void deleteAuthor(AuthorDto authorDto);

    /**
     * Sets authors' expire date as current date. After that author
     * counts as deleted and cannot be added as an author in new news.
     * But it still be possible to see author in already created news messages.
     *
     * @param authorId id of author to be deleted.
     */
    void deleteAuthor(Long authorId);
}
