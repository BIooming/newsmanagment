package by.epam.lab.fedarenka.newsmanagment.service;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.TagDto;

import java.util.List;

/**
 * Identity of the class of an TagService instance can perform CRUD operations
 * with Tag entity.
 */
public interface TagService {

    /**
     * Adds new tag into the database.
     *
     * @param tag entity of the Tag which will be saved  in database.
     * @return saved tag with specified id.
     */
    TagDto addTag(TagDto tag);

    /**
     * Updates tag data in the database by replacing existing values with
     * new ones from entity given in parameter
     *
     * @param tag entity with new field values. Should have same id
     *            value as an old version.
     * @return updated Tag entity.
     */
    TagDto updateTag(TagDto tag);

    /**
     * Search Author database by corresponding id given in parameter.
     *
     * @param tagId id of tag to be found in database.
     * @return entity fully filled with data from database.
     */
    TagDto getTag(Long tagId);

    /**
     * Returns lis of all tags stored in database in the moment.
     *
     * @return List of {@link TagDto} entities filled  with data from database.
     */
    List<TagDto> getAllTags();

    /**
     * Deletes from database tag which given in parameter.
     *
     * @param tagId id of news to be deleted.
     */
    void deleteTag(Long tagId);

    /**
     * Deletes from database tag which given in parameter.
     * Tag entity given in parameter should have an id specified.
     *
     *  @param tag entity to be deleted.
     */
    void deleteTag(TagDto tag);
}
