package by.epam.lab.fedarenka.newsmanagment.dal;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;

import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public interface AuthorDao {

    AuthorDto getAuthor(Long id) throws DalException;

    AuthorDto create(AuthorDto author) throws DalException;

    AuthorDto update(AuthorDto author) throws DalException;

    List<AuthorDto> listAuthors() throws DalException;

    void delete(Long id) throws DalException;
}
