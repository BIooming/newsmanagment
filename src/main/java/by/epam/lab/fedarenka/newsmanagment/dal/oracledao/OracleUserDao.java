package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.UserDto;
import by.epam.lab.fedarenka.newsmanagment.dal.DalException;
import by.epam.lab.fedarenka.newsmanagment.dal.UserDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleUserDao extends OracleDao implements UserDao {
    private static final String SQL_CREATE = "INSERT INTO User(USER_NAME,LOGIN,PASSWORD) VALUES(?,?,?);";
    private static final String SQL_SEARCH_ALL = "SELECT * FROM User;";
    private static final String SQL_SEARCH_BY_LOGIN = "SELECT * FROM User WHERE LOGIN=?;";
    private static final String SQL_SEARCH_BY_ID = "SELECT * FROM User WHERE USER_ID=?;";
    private static final String SQL_UPDATE = "UPDATE User SET USER_NAME=?,LOGIN=?,PASSWORD=? WHERE USER_ID=?;";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM User WHERE USER_ID=?;";
    private static final String SQL_DELETE_BY_LOGIN = "DELETE FROM User WHERE LOGIN=?;";

    private static final String STR_USER_ID = "USER_ID";
    private static final String STR_USER_NAME = "USER_NAME";
    private static final String STR_LOGIN = "LOGIN";
    private static final String STR_PASSWORD = "PASSWORD";

    private static final int NULL = 0;

    public UserDto create(UserDto user) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_CREATE, user.getUserName(), user.getLogin(),user.getPassword());
        user = getUserFromResultSet(resultSet).get(NULL);
        return user;
    }

    public UserDto getUser(Long id) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_BY_ID, id);
        UserDto user = getUserFromResultSet(resultSet).get(NULL);
        return user;
    }

    public UserDto getUser(String login) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_BY_LOGIN, login);
        UserDto user = getUserFromResultSet(resultSet).get(NULL);
        return user;
    }

    public UserDto update(UserDto user) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_UPDATE,
                user.getUserName(), user.getLogin(),user.getPassword(),user.getPassword());
        user = getUserFromResultSet(resultSet).get(NULL);
        return user;
    }

    public List<UserDto> listUsers() throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_ALL);
        List<UserDto> users = getUserFromResultSet(resultSet);
        return users;
    }

    public void delete(Long id) throws DalException {
        executeSqlRequest(SQL_DELETE_BY_ID, id);
    }

    public void delete(String login) throws DalException {
        executeSqlRequest(SQL_DELETE_BY_LOGIN, login);
    }

    private List<UserDto> getUserFromResultSet(ResultSet resultSet) throws DalException{
        try {
            List<UserDto> users = new ArrayList<UserDto>();
            while (resultSet.next()){
                UserDto user = new UserDto();
                user.setId(resultSet.getLong(STR_USER_ID));
                user.setUserName(resultSet.getString(STR_USER_NAME));
                user.setLogin(resultSet.getString(STR_LOGIN));
                user.setPassword(resultSet.getString(STR_PASSWORD));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }
}
