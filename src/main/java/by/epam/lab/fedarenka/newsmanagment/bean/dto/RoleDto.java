package by.epam.lab.fedarenka.newsmanagment.bean.dto;

import by.epam.lab.fedarenka.newsmanagment.bean.Role;

import java.io.Serializable;

/**
 *
 */
public class RoleDto implements Serializable {
    private static final Long serialVersionUID = 1L;
    private Long userId;
    private Role name;

    public RoleDto(Role name, Long userId) {
        this.name = name;
        this.userId = userId;
    }

    public RoleDto(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleDto roleDto = (RoleDto) o;

        if (userId != null ? !userId.equals(roleDto.userId) : roleDto.userId != null) return false;
        return name == roleDto.name;

    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "userId=" + userId +
                ", name=" + name +
                '}';
    }

    public static Long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Role getName() {
        return name;
    }

    public void setName(Role name) {
        this.name = name;
    }
}
