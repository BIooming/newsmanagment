package by.epam.lab.fedarenka.newsmanagment.dal.factoryimpl;

import by.epam.lab.fedarenka.newsmanagment.dal.*;
import by.epam.lab.fedarenka.newsmanagment.dal.oracledao.*;

/**
 * Created by blooming on 23.6.16.
 */
public final class OracleDaoFactory extends DaoFactory {
    private static OracleDaoFactory instance;
    //@TODO Spring DI
    public static OracleDaoFactory getInstance(){
        return instance;
    }

    public AuthorDao getAuthorDao() {
        //return OracleAuthorDao.getInstance();
        return null;
    }

    public CommentsDao getCommentsDao() {
        return null;//OracleCommentsDao.getInstance();
    }

    public NewsDao getNewsDao() {
        return null;//OracleNewsDao.getInstance();
    }

    public NewsAuthorDao getNewsAuthorDao() {
        return null;//OracleNewsAuthorDao.getInstance();
    }

    public NewsTagDao getNewsTagDao() {
        return null;//OracleNewsTagDao.getInstance();
    }

    public TagDao getTagDao() {
        return null;//OracleTagDao.getInstance();
    }

    public UserDao getUserDao() {
        return null;//OracleUserDao.getInstance();
    }

    private OracleDaoFactory(){
        instance = new OracleDaoFactory();
    }
}
