package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.dal.DalException;
import by.epam.lab.fedarenka.newsmanagment.dal.NewsAuthorDao;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleNewsAuthorDao extends OracleDao implements NewsAuthorDao {
    private static final String SQL_CREATE = "INSERT INTO News_Author(NEWS_ID,AUTHOR_ID) VALUES(?,?);";
    private static final String SQL_DELETE_NEWS_AUTHOR = "DELETE * FROM News_Author WHERE NEWS_ID=? AND AUTHOR_ID=?;";
    private static final String SQL_DELETE_BY_AUTHOR = "DELETE * FROM News_Author WHERE AUTHOR_ID=?;";
    private static final String SQL_DELETE_BY_NEWS = "DELETE * FROM News_Author WHERE NEWS_ID=?;";

    public void createNewsAuthor(Long newsId, Long authorId) throws DalException {
        executeSqlRequest(SQL_CREATE, newsId, authorId);
    }

    public void deleteNewsAuthor(Long newsId, Long authorId) throws DalException {
        executeSqlRequest(SQL_DELETE_NEWS_AUTHOR, newsId, authorId);
    }

    public void deleteAllNewsForAuthor(Long authorId) throws DalException {
        executeSqlRequest(SQL_DELETE_BY_AUTHOR, authorId);
    }

    public void deleteNews(Long newsId) throws DalException {
        executeSqlRequest(SQL_DELETE_NEWS_AUTHOR, newsId);
    }
}
