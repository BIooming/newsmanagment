package by.epam.lab.fedarenka.newsmanagment.service;

import by.epam.lab.fedarenka.newsmanagment.bean.SearchCriteria;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;

import java.util.List;

/**
 * Defines common behavior of news processing service.
 * Process both {@link NewsDto} and {@link by.epam.lab.fedarenka.newsmanagment.bean.NewsMessage}.
 * Guarantees that implementation will be able to perform basic CRUD operations with NewsDto entity.
 * Provides method for searching News using {@link SearchCriteria}.
 *
 * @see SearchCriteria
 * @see NewsDto
 * @see List
 * @see by.epam.lab.fedarenka.newsmanagment.bean.NewsMessage
 */
public interface NewsService {

    /**
     * Performs adding news entity into the database.
     *
     * @param news {@link NewsDto} entity to be added into the database.
     * @return news created with the corresponding id which was assigned by database.
     */
    NewsDto addNews(NewsDto news);

    /**
     * Performs editing of news data in database. Replaces existing values of fields in database
     * by new ones from the News entity in parameter.
     *
     * @param news {@link NewsDto} entity which contains edits to be written in database.
     *             Should contain edited data and id of the news old version.
     * @return updated news.
     */
    NewsDto updateNews(NewsDto news);

    /**
     * Performs search in database by corresponding id given in parameter.
     *
     * @param newsId id of required News.
     * @return {@link NewsDto} entity of required News.
     */
    NewsDto getNews(Long newsId);

    /**
     * Returns list of all News in database sorted by count of comments
     * (most commented goes first).
     *
     * @return {@link List} of News sorted by count of comments.
     */
    List<NewsDto> getAllNewsSorted();

    /**
     * Searches all news matching {@link SearchCriteria} given in parameter.
     *
     * @param searchCriteria specify parameters of search.
     *                       May contain author entity and/or list of tags.
     * @return list of News corresponding the search criteria.
     */
    List<NewsDto> searchNews(SearchCriteria searchCriteria);

    /**
     * Counts all news stored in database.
     *
     * @return count of news.
     */
    Integer getNewsCount();

    /**
     * Deletes from database news which was given in parameter.
     * NewsDto entity given in parameter should have an id specified.
     *
     * @param news entity to delete.
     */
    void deleteNews(NewsDto news);

    /**
     * Deletes from database news by id specified in parameter.
     *
     * @param newsId id of news to delete.
     */
    void deleteNews(Long newsId);
}
