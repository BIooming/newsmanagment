package by.epam.lab.fedarenka.newsmanagment.dal;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.UserDto;

import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public interface UserDao {

    UserDto create(UserDto user) throws DalException;

    UserDto getUser(Long id) throws DalException;

    UserDto getUser(String login) throws DalException;

    UserDto update(UserDto user) throws DalException;

    List<UserDto> listUsers() throws DalException;

    void delete(Long id) throws DalException;

    void delete(String login) throws DalException;
}
