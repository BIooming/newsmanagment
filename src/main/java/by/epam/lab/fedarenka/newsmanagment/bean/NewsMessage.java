package by.epam.lab.fedarenka.newsmanagment.bean;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.CommentDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.TagDto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public class NewsMessage implements Serializable {
    private static final Long serialVersionUID = 1L;
    private NewsDto news;
    private List<CommentDto> comments;
    private List<AuthorDto> authors;
    private List<TagDto> tags;

    public NewsMessage(NewsDto news, List<CommentDto> comments, List<AuthorDto> authors, List<TagDto> tags) {
        this.news = news;
        this.comments = comments;
        this.authors = authors;
        this.tags = tags;
    }

    public NewsMessage(List<CommentDto> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsMessage that = (NewsMessage) o;

        if (news != null ? !news.equals(that.news) : that.news != null) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (authors != null ? !authors.equals(that.authors) : that.authors != null) return false;
        return tags != null ? tags.equals(that.tags) : that.tags == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsMessage{" +
                "news=" + news +
                ", comments=" + comments +
                ", authors=" + authors +
                ", tags=" + tags +
                '}';
    }

    public NewsDto getNews() {
        return news;
    }

    public void setNews(NewsDto news) {
        this.news = news;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    public void setComments(List<CommentDto> comments) {
        this.comments = comments;
    }

    public List<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDto> authors) {
        this.authors = authors;
    }

    public List<TagDto> getTags() {
        return tags;
    }

    public void setTags(List<TagDto> tags) {
        this.tags = tags;
    }
}
