package by.epam.lab.fedarenka.newsmanagment.dal;

import by.epam.lab.fedarenka.newsmanagment.dal.factoryimpl.OracleDaoFactory;

/**
 * Created by blooming on 22.6.16.
 */
public abstract class DaoFactory {
    //@TODO perform getting factory implementation via Spring DI
    public static DaoFactory getDaoFactory(){
        return OracleDaoFactory.getInstance();
    }

    public abstract AuthorDao getAuthorDao();

    public abstract CommentsDao getCommentsDao();

    public abstract NewsDao getNewsDao();

    public abstract NewsAuthorDao getNewsAuthorDao();

    public abstract NewsTagDao  getNewsTagDao();

    public abstract TagDao getTagDao();

    public abstract UserDao getUserDao();
}
