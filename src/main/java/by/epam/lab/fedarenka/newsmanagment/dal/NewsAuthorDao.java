package by.epam.lab.fedarenka.newsmanagment.dal;

/**
 * Created by blooming on 22.6.16.
 */
public interface NewsAuthorDao {

    void createNewsAuthor(Long newsId, Long authorId) throws DalException;

    void deleteNewsAuthor(Long newsId, Long author) throws DalException;

    void deleteAllNewsForAuthor(Long authorId) throws DalException;

    void deleteNews(Long newsId)throws DalException;
}
