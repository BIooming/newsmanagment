package by.epam.lab.fedarenka.newsmanagment.bean.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by blooming on 22.6.16.
 */
public class CommentDto
        implements Serializable {
    private static final Long serialVersionUID = 1L;
    private Long id;
    private Long newsId;
    private String text;
    private Timestamp creationDate;

    public CommentDto(Long id, Long newsId, String text, Timestamp creationDate) {
        this.id = id;
        this.newsId = newsId;
        this.text = text;
        this.creationDate = creationDate;
    }

    public CommentDto() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentDto that = (CommentDto) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (newsId != null ? !newsId.equals(that.newsId) : that.newsId != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        return creationDate != null ? creationDate.equals(that.creationDate) : that.creationDate == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CommentDto{" +
                "id=" + id +
                ", newsId=" + newsId +
                ", text='" + text + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}
