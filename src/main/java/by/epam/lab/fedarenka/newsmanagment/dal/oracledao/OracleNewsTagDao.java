package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.dal.DalException;
import by.epam.lab.fedarenka.newsmanagment.dal.NewsTagDao;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleNewsTagDao extends OracleDao implements NewsTagDao {
    private static final String SQL_INSERT = "INSERT INTO News_Tag(NEWS_ID, TAG_ID);";
    private static final String SQL_DELETE_BY_NEWS = "DELETE * FROM News_Tag WHERE NEWS_ID=?;";
    private static final String SQL_DELETE_BY_TAG = "DELETE * FROM News_Tag WHERE TAG_ID=?;";
    private static final String SQL_DELETE = "DELETE * FROM News_Tag NEWS_ID=? AND TAG_ID=?;";

    public void addNewsTag(Long newsId, Long tagId) throws DalException {
        executeSqlRequest(SQL_INSERT, newsId, tagId);
    }

    public void deleteNewsTag(Long newsId, Long tagId) throws DalException {
        executeSqlRequest(SQL_DELETE, newsId, tagId);
    }

    public void deleteAllTagsForNews(Long newsId) throws DalException {
        executeSqlRequest(SQL_DELETE_BY_NEWS, newsId);
    }

    public void deleteTagFromAllNews(Long tagId) throws DalException {
        executeSqlRequest(SQL_DELETE_BY_TAG, tagId);
    }
}
