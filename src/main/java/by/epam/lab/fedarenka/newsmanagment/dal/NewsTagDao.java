package by.epam.lab.fedarenka.newsmanagment.dal;

/**
 * Created by blooming on 22.6.16.
 */
public interface NewsTagDao {

    void addNewsTag(Long newsId, Long tagId) throws DalException;

    void deleteNewsTag(Long newsId, Long tagId) throws DalException;

    void deleteAllTagsForNews(Long newsId) throws DalException;

    void deleteTagFromAllNews(Long tagId) throws DalException;
}
