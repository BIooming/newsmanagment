package by.epam.lab.fedarenka.newsmanagment.bean.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by blooming on 22.6.16.
 */
public class AuthorDto implements Serializable {
    private static final Long serialVersionUID = 1L;
    private Long id;
    private String name;
    private Timestamp expired;

    public AuthorDto(Long id, String name, Timestamp expired) {
        this.id = id;
        this.name = name;
        this.expired = expired;
    }

    public AuthorDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public AuthorDto() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthorDto authorDto = (AuthorDto) o;

        if (id != null ? !id.equals(authorDto.id) : authorDto.id != null) return false;
        if (name != null ? !name.equals(authorDto.name) : authorDto.name != null) return false;
        return expired != null ? expired.equals(authorDto.expired) : authorDto.expired == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuthorDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", expired=" + expired +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }
}
