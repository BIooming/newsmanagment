package by.epam.lab.fedarenka.newsmanagment.service.impl;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;
import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;
import by.epam.lab.fedarenka.newsmanagment.service.AuthorService;

import java.util.List;

/**
 * Created by Uladzislau_Fedarenk1 on 7/12/2016.
 */
public class AuthorServiceImpl implements AuthorService {
    public AuthorDto addAuthor(AuthorDto author) {
        return null;
    }

    public AuthorDto updateAuthor(AuthorDto author) {
        return null;
    }

    public AuthorDto getAuthor(Long authorId) {
        return null;
    }

    public List<AuthorDto> getAllAuthors() {
        return null;
    }

    public void detachAuthorFromNews(AuthorDto author, NewsDto news) {

    }

    public void detachAuthorFromNews(Long authorId, Long newsId) {

    }

    public void deleteAuthor(AuthorDto authorDto) {

    }

    public void deleteAuthor(Long authorId) {

    }
}
