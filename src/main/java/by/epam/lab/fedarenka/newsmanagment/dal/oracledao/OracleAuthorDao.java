package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.AuthorDto;
import by.epam.lab.fedarenka.newsmanagment.dal.AuthorDao;
import by.epam.lab.fedarenka.newsmanagment.dal.DalException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleAuthorDao extends OracleDao implements AuthorDao {
    private static final String SQL_SEARCH_BY_ID = "SELECT AUTHOR_ID,AUTHOR_NAME,EXPIRED FROM Author WHERE 'AUTHOR_ID'=?;";
    private static final String SQL_SEARCH_ALL = "SELECT AUTHOR_ID,AUTHOR_NAME,EXPIRED FROM Author;";
    private static final String SQL_CREATE = "INSERT INTO Author(AUTHOR_NAME,EXPIRED) VALUES(?,?);";
    private static final String SQL_DELETE = "DELETE FROM Author WHERE AUTHOR=?;";
    private static final String SQL_UPDATE = "UPDATE Author SET AUTHOR_NAME=?,EXPIRE=? WHERE AUTHOR_ID=?;";

    private static final String STR_AUTHOR_NAME = "AUTHOR_NAME";
    private static final String STR_EXPIRED = "EXPIRED";
    private static final String STR_AUTHOR_ID = "AUTHOR_ID";

    private static final int NULL = 0;
    private static final int ONE = 1;

    public AuthorDto getAuthor(Long id) throws DalException {
        ResultSet resultSet =
                executeSqlRequest(SQL_SEARCH_BY_ID, id);
        List<AuthorDto> authorDtoList = getAuthorsFromResultSet(resultSet);
        AuthorDto author = authorDtoList.get(NULL);
        return author;
    }

    public AuthorDto create(AuthorDto author) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_CREATE,
                author.getName(),
                author.getExpired());
        List<AuthorDto> authorDtoList = getAuthorsFromResultSet(resultSet);
        author = authorDtoList.get(NULL);
        return author;
    }

    public AuthorDto update(AuthorDto author) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_UPDATE,
                author.getName(),
                author.getExpired(),
                author.getId());
        List<AuthorDto> authorDtoList = getAuthorsFromResultSet(resultSet);
        author = authorDtoList.get(NULL);
        return author;
    }

    public List<AuthorDto> listAuthors() throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_ALL);
        List<AuthorDto> authorDtoList = getAuthorsFromResultSet(resultSet);
        return authorDtoList;
    }

    public void delete(Long id) throws DalException {
        executeSqlRequest(SQL_DELETE, id);
    }

    private List<AuthorDto> getAuthorsFromResultSet(ResultSet resultSet) throws DalException {
        try {
            List<AuthorDto> authors = new ArrayList<AuthorDto>();
            while (resultSet.next()) {
                AuthorDto author = new AuthorDto();
                author.setId(resultSet.getLong(STR_AUTHOR_ID));
                author.setName(resultSet.getString(STR_AUTHOR_NAME));
                author.setExpired(resultSet.getTimestamp(STR_EXPIRED));

                authors.add(author);
            }
            return authors;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }
}
