package by.epam.lab.fedarenka.newsmanagment.dal.oracledao;

import by.epam.lab.fedarenka.newsmanagment.bean.dto.NewsDto;
import by.epam.lab.fedarenka.newsmanagment.dal.DalException;
import by.epam.lab.fedarenka.newsmanagment.dal.NewsDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blooming on 22.6.16.
 */
public final class OracleNewsDao extends OracleDao implements NewsDao {
    private static final String SQL_CREATE =
            "INSERT INTO News(TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) VALUES(?,?,?,?,?);";
    private static final String SQL_UPDATE =
            "UPDATE News SET TITLE=?,SHORT_TEXT=?,FULL_TEXT=?,CREATION_DATE=?,MODIFICATION_DATE=? WHERE NEWS_ID=?";
    private static final String SQL_SEARCH_BY_ID = "SELECT * FROM News WHERE News_ID=?;";
    private static final String SQL_SEARCH_ALL = "SELECT * FROM News;";
    private static final String SQL_COUNT = "SELECT COUNT(*) AS NUMBER_OF_NEWS FROM News;";
    private static final String SQL_DELETE = "DELETE * FROM News WHERE NEWS_ID=?;";

    private static final String STR_NEWS_ID = "NEWS_ID";
    private static final String STR_TITLE = "TITLE";
    private static final String STR_SHORT_TEXT = "SHORT_TEXT";
    private static final String STR_FULL_TEXT = "FULL_TEXT";
    private static final String STR_CREATION_DATE = "CREATION_DATE";
    private static final String STR_MODIFICATION_DATE = "MODIFICATION_DATE";
    private static final String STR_NUMBER_OF_NEWS = "NUMBER_OF_NEWS";

    private static final int NULL = 0;

    public NewsDto create(NewsDto news) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_CREATE,
                news.getTitle(),
                news.getShortText(),
                news.getFullText(),
                news.getCreationDate(),
                news.getModificationDate());
        news = getNewsFromResultSet(resultSet).get(NULL);
        return news;
    }

    public NewsDto getNews(Long newsId) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_BY_ID, newsId);
        NewsDto news = getNewsFromResultSet(resultSet).get(NULL);
        return news;
    }

    public NewsDto update(NewsDto news) throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_UPDATE,
                news.getTitle(),
                news.getShortText(),
                news.getFullText(),
                news.getCreationDate(),
                news.getModificationDate());
        news = getNewsFromResultSet(resultSet).get(NULL);
        return news;
    }

    public List<NewsDto> listAllNews() throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_SEARCH_ALL);
        List<NewsDto> newsList = getNewsFromResultSet(resultSet);
        return newsList;
    }

    public int count() throws DalException {
        ResultSet resultSet = executeSqlRequest(SQL_COUNT);
        try {
            int numberOfNews = 0;
            while (resultSet.next()) {
                numberOfNews = resultSet.getInt(STR_NUMBER_OF_NEWS);
            }
            return numberOfNews;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }

    public void delete(Long newsId) throws DalException {
        executeSqlRequest(SQL_DELETE);
    }

    private List<NewsDto> getNewsFromResultSet(ResultSet resultSet) throws DalException {
        try {
            List<NewsDto> newsList = new ArrayList<NewsDto>();
            while (resultSet.next()) {
                NewsDto news = new NewsDto();
                news.setId(resultSet.getLong(STR_NEWS_ID));
                news.setTitle(resultSet.getString(STR_TITLE));
                news.setShortTitle(resultSet.getString(STR_SHORT_TEXT));
                news.setFullText(resultSet.getString(STR_FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(STR_CREATION_DATE));
                news.setModificationDate(resultSet.getDate(STR_MODIFICATION_DATE));
                newsList.add(news);
            }
            return newsList;
        } catch (SQLException e) {
            throw new DalException(e);
        }
    }
}
